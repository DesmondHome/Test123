﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ClassLibrary1Test
{
	[TestFixture]
    public class Class1
    {
		[Test]
		public void TestSuccess()
		{
			Assert.AreEqual(1, 1);
		}

		[Test]
		public void TestFailure()
		{
			Assert.AreEqual(1, 2);
		}
    }
}
